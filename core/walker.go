package core

import (
	"log"
	"os"

	opt "gitlab.com/joshrasmussen/mono-ci/options"
)

// Visitor is an object that knows how to visit
// a particular service
type Visitor interface {
	VisitService(s *Service) error
}

// Navigator is effectively a "stack" and can navigate
// to a particular service (push) and then return from the current
// service (pop)
type Navigator interface {
	NavigateToService(s *Service) error
	Return() error
}

// Walker encapsulates objects that
// both know how to go to a service as
// well as knows what to do when it gets there
type Walker struct {
	Navigator Navigator
	Visitor   Visitor
	Options   opt.Options
}

// Walk goes through services and "visits" each service
func (w *Walker) Walk(services []Service) {
	var err error

	for _, s := range services {
		w.Navigator.NavigateToService(&s)

		err = w.Visitor.VisitService(&s)
		if err != nil {
			log.Println(err)
			if w.Options.FailFast {
				os.Exit(1)
			}
		}

		err = w.Navigator.Return()
		if err != nil {
			return
		}
	}
}
