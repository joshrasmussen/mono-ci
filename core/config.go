package core

// Config defines information in
// the yaml file for running tests and checks
// for a given monorepo
type Config struct {
	Root     string             `yaml:"root"`
	Services map[string]Service `yaml:"services,flow"`
}

// GetServices reduces the services map to a slice
// using the keys to replace a service path if not present
func (c *Config) GetServices() []Service {
	result := make([]Service, 0, len(c.Services))

	for key, value := range c.Services {
		if value.Path == "" {
			value.Path = key
		}

		result = append(result, value)
	}

	return result
}
