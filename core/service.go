package core

import "fmt"

// Service describes a given sub-project
// in the mono repo
type Service struct {
	Path  string   `yaml:"path"`
	Check []string `yaml:"check,flow"`
	Test  []string `yaml:"test,flow"`
}

// GetCommand is a factory method that determines which command to generate
func (s *Service) GetCommand(command string) (result []string, err error) {
	switch command {
	case "test":
		result = s.Test
	case "check":
		result = s.Check
	default:
		err = fmt.Errorf("%s is an invalid command", command)
	}

	return
}
