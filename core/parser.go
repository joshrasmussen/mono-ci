package core

import (
	"io"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

// GetConfig finds mono.yaml or mono.yml
// and reads in the data into the Config struct
func GetConfig(input io.Reader) *Config {
	c := Config{}

	data, err := ioutil.ReadAll(input)

	if err != nil {
		log.Panicf("Unable to read file: %v", err)
	}

	err = yaml.Unmarshal(data, &c)

	if err != nil {
		log.Panicf("Error parsing config file: %v", err)
	}

	return &c
}
