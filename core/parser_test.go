package core

import (
	"reflect"
	"strings"
	"testing"
)

const mockInput = `
root: ROOT
services:
  db: 
    test: [ "shell" ]
`

var expected Config = Config{
	Root: "ROOT",
	Services: map[string]Service{
		"db": Service{
			Test: []string{"shell"},
		},
	},
}

func Test_GetConfig(t *testing.T) {
	input := strings.NewReader(mockInput)

	c := GetConfig(input)

	if !reflect.DeepEqual(*c, expected) {
		t.Errorf("Parsed YAML didn't match expected form %s", expected)
	}
}
