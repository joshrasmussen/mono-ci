package core

import "testing"

type TestVisitor struct {
	calls int
	trace string
}

func (v *TestVisitor) VisitService(s *Service) error {
	v.trace += " > " + s.Path
	v.calls++

	return nil
}

type TestNavigator struct {
	navCount    int
	returnCount int
}

func (n *TestNavigator) NavigateToService(s *Service) error {
	n.navCount++

	return nil
}

func (n *TestNavigator) Return() error {
	n.returnCount++

	return nil
}

var s []Service = []Service{
	Service{
		Path: "./first",
	},
	Service{
		Path: "./second",
	},
}

func Test_Walk(t *testing.T) {
	v := &TestVisitor{}
	n := &TestNavigator{}

	w := Walker{
		Navigator: n,
		Visitor:   v,
	}

	w.Walk(s)

	expectedTrace := " > ./first > ./second"

	if v.calls != 2 || v.trace != expectedTrace || n.navCount != 2 || n.returnCount != 2 {
		t.Errorf("Expected to visit 2 services and have trace: %s", expectedTrace)
	}
}
