package core

import "testing"

func Test_GetServices(t *testing.T) {
	c := Config{
		Root: "ROOT",
		Services: map[string]Service{
			"db": Service{
				Test: []string{"shell"},
			},
		},
	}

	result := c.GetServices()

	if result[0].Path != "db" {
		t.Errorf("Expected path to be set to key but got %s", result[0].Path)
	}
}

func Test_GetServices_OverridePath(t *testing.T) {
	c := Config{
		Root: "ROOT",
		Services: map[string]Service{
			"db": Service{
				Path: "myPath",
				Test: []string{"shell"},
			},
		},
	}

	result := c.GetServices()

	if result[0].Path != "myPath" {
		t.Errorf("Expected path to be set to key but got %s", result[0].Path)
	}
}
