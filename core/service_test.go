package core

import (
	"reflect"
	"testing"
)

func TestService_GetCommand(t *testing.T) {
	type fields struct {
		Path  string
		Check []string
		Test  []string
	}
	type args struct {
		command string
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantResult []string
		wantErr    bool
	}{
		{
			name: "get test",
			fields: fields{
				Check: []string{"my_check"},
				Test:  []string{"my_test"},
			},
			args: args{
				command: "test",
			},
			wantResult: []string{"my_test"},
			wantErr:    false,
		},
		{
			name: "get check",
			fields: fields{
				Check: []string{"my_check"},
				Test:  []string{"my_test"},
			},
			args: args{
				command: "check",
			},
			wantResult: []string{"my_check"},
			wantErr:    false,
		},
		{
			name: "invalid command",
			fields: fields{
				Check: []string{"my_check"},
				Test:  []string{"my_test"},
			},
			args: args{
				command: "foo",
			},
			wantResult: nil,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Service{
				Path:  tt.fields.Path,
				Check: tt.fields.Check,
				Test:  tt.fields.Test,
			}
			gotResult, err := s.GetCommand(tt.args.command)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.GetCommand() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Service.GetCommand() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
