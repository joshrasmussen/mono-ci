package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"gitlab.com/joshrasmussen/mono-ci/core"
	"gitlab.com/joshrasmussen/mono-ci/options"
)

// Executor wraps around a command and then
// executes the command in the service
type Executor struct {
	cmd     string
	options *options.Options
}

// VisitService executes the command in the given service
func (e *Executor) VisitService(s *core.Service) (err error) {
	var stdio, stderr io.ReadCloser
	var out []byte
	command, err := s.GetCommand(e.cmd)

	if err != nil {
		log.Printf("%v", err)
		return
	}

	log.Printf("==========================================")
	log.Printf("Starting Service: %s", s.Path)
	log.Printf("==========================================")

	for _, c := range command {
		log.Printf("%s > Running: %s", s.Path, c)
		cmd := exec.Command("sh", "-c", c)
		stdio, err = cmd.StdoutPipe()
		if err != nil {
			return fmt.Errorf("%s > Error: %s", s.Path, err)
		}

		stderr, err = cmd.StderrPipe()
		if err != nil {
			return fmt.Errorf("%s > Error: %s", s.Path, err)
		}

		err = cmd.Start()
		if err != nil {
			return fmt.Errorf("%s > Error: %s", s.Path, err)
		}

		out, _ = ioutil.ReadAll(stdio)
		log.Printf("%s > Output: %s", s.Path, string(out))

		out, _ = ioutil.ReadAll(stderr)

		err = cmd.Wait()
		if err != nil {
			return fmt.Errorf("%s > Error: %s", s.Path, err)
		}

		if len(out) > 0 {
			if e.options.IgnoreStderr {
				log.Printf("%s > Error: %s", s.Path, string(out))
			} else {
				return fmt.Errorf("%s > Error: %s", s.Path, string(out))
			}
		}
	}

	log.Printf("%s > Success!\n", s.Path)

	return
}

// FsNavigator logs each step of navigation (i.e. breadcrumbs)
// and logs errors that occured
type FsNavigator struct {
	Failed      []core.Service
	Breadcrumbs []string
}

// NavigateToService gos to the current service directory using os calls
// If there is an error then we will navigate back to the previous location
// and return the error
func (fs *FsNavigator) NavigateToService(s *core.Service) error {
	err := os.Chdir(s.Path)

	if err != nil {
		fs.Failed = append(fs.Failed, *s)
		return err
	}

	newLocation, err := os.Getwd()

	if err != nil {
		// Go back to the last location before leaving
		os.Chdir(fs.Breadcrumbs[len(fs.Breadcrumbs)-1])
		fs.Failed = append(fs.Failed, *s)
		return err
	}

	fs.Breadcrumbs = append(fs.Breadcrumbs, newLocation)

	return nil
}

// Return goes back to the last navigated location
// if there is one to go back to. I theory this
// should go back to an exisitng directory so it wouldn't
// existentially fail.
func (fs *FsNavigator) Return() error {
	if len(fs.Breadcrumbs) == 0 {
		return fmt.Errorf("Nothing to return to")
	}

	index := len(fs.Breadcrumbs) - 1
	fs.Breadcrumbs = fs.Breadcrumbs[:index]

	err := os.Chdir(fs.Breadcrumbs[len(fs.Breadcrumbs)-1])

	if err != nil {
		panic(err)
	}

	return nil
}

func main() {
	// Read config file
	file, err := os.Open("mono.yaml")
	if err != nil {
		log.Panicf("Error trying to read mono.yaml: %v", err)
	}
	config := core.GetConfig(file)
	defer file.Close()

	// Parse options
	cmd, options, err := options.ParseOptions(os.Args[1:])
	if err != nil {
		log.Panicf("Error parsing arguments: %v", err)
	}

	// Go to the root directory
	if config.Root != "" {
		err = os.Chdir(config.Root)
		if err != nil {
			log.Panicf("Something went wrong navigating to root: %v", err)
		}
	}

	rootDir, err := os.Getwd()

	if err != nil {
		log.Panicf("Something went wrong getting the working directory: %v", err)
	}

	// Create our navigators and visitors
	nav := &FsNavigator{
		Breadcrumbs: []string{rootDir},
	}
	executor := &Executor{
		cmd:     cmd,
		options: options,
	}
	walker := core.Walker{
		Navigator: nav,
		Visitor:   executor,
		Options:   *options,
	}

	walker.Walk(config.GetServices())
}
