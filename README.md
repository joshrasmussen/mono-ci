# Mono CI

MonoCI is a CLI tool for specifying a configuration to run tests and checks on a group of 
services. This script is platform agnostic so one service could be a [Yarn Package](https://yarnpkg.com) 
and another could be a [GoLang Package](https://https://golang.org) and the script will find both 
and run the specified script. 

## Getting Started

### Install the CLI tool

```bash
go get gitlab.com/joshrasmussen/mono-ci
```

### Create a mono.yaml file

The script looks for a `mono.yaml` file as its run configuration. 

#### Root Schema

| Key | Type | Default | Description |
|:----|:----|:--------|:------------|
| `root` | `string` | `.` | Where MonoCI should begin looking for services |
| `services` | `map[string]Service` | `nil` | A map of strings mapped to a service configuration |

#### Service Schema

| Key | Type | Default | Description |
|:----|:----|:--------|:------------|
| `path` | `string` | the key this service is mapped to | The path to look for the service in relative to root | 
| `type` | `shell, yarn, go` | `shell` | Determines how to run the service |
| `test` | `string` | `""` | If type == shell, then run this script when in test mode | 
| `check` | `string` | `""` | If type == shell, then run this script when in check mode | 

#### Example 

```yaml
root: ./services
services:
  api:
    path: ./my_api
    type: shell
    test: 'test.sh'
    check: 'check.sh'
  app:
    path: ./my_app
    type: yarn
```

### Running the script 

```bash
mono-ci test -fail
mono-ci check
```

#### Flags

| Name | Description |
|:-----|:------------|
| fail | When set the script will fail upon finding a single non-zero exit status |

## Inspiration 

- [Docker Compose](https://docs.docker.com/compose/)
