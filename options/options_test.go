package options

import "testing"

func Test_ParseOptions_ProvidedOptions(t *testing.T) {
	args := []string{"check", "--fail", "-ignore_stderr"}
	cmd, result, err := ParseOptions(args)

	expect := Options{
		FailFast:     true,
		IgnoreStderr: true,
	}

	if err != nil || *result != expect || cmd != "check" {
		t.Errorf("Parsed options did not match expected struct: %v", expect)
	}
}

func Test_ParseOptions_DefaultOptions(t *testing.T) {
	args := []string{"test"}
	cmd, result, err := ParseOptions(args)

	expect := Options{
		FailFast:     false,
		IgnoreStderr: false,
	}

	if err != nil || *result != expect || cmd != "test" {
		t.Errorf("Parsed options did not match expected struct: %v", expect)
	}
}

func Test_ParseOptions_NoCommand(t *testing.T) {
	args := []string{}
	_, _, err := ParseOptions(args)

	if err == nil {
		t.Error("Expected an error to be returned with no arguments")
	}
}

func Test_ParseOptions_InvalidCommand(t *testing.T) {
	args := []string{"-fail"}
	_, _, err := ParseOptions(args)

	if err == nil {
		t.Error("Expected an error to be returned with an invalid command")
	}
}
