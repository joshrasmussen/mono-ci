package options

import (
	"flag"
	"fmt"
)

// Options represents configuration
// that can be used to alter how we
// run our services
type Options struct {
	FailFast     bool
	IgnoreStderr bool
}

func isValidCommand(cmd string) bool {
	validCommands := []string{"test", "check"}

	for _, value := range validCommands {
		if value == cmd {
			return true
		}
	}

	return false
}

// ParseOptions create the Options struct given
// a string slice.
// This slice is expected to be the same as os.Args
func ParseOptions(args []string) (cmd string, options *Options, err error) {
	// Make sure command is valid
	if len(args) == 0 {
		err = fmt.Errorf("Must provide at least a command (test or check)")
		return
	}

	cmd = args[0]

	if !isValidCommand(cmd) {
		err = fmt.Errorf("%s is an invalid command (must be test or check)", cmd)
		return
	}

	options = &Options{}

	fs := flag.NewFlagSet("MonoCI Options", flag.ExitOnError)

	fs.BoolVar(&options.FailFast, "fail", false, "Suite should fail upon finding the first failure")
	fs.BoolVar(&options.IgnoreStderr, "ignore_stderr", false, "Suite should not fail if stderr is not empty")

	fs.Parse(args[1:])

	return
}
